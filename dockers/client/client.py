import socket
from time import sleep

interval = 2 #seconds

def get_value(server):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(server)
    s.settimeout(0.5)
    s.send('Hi'.encode())
    val = s.recv(1024)
    s.close()
    count = round(int(val.decode())/1000,2)
    return str(count)+'k'

count_arm, count_amd = '0k', '0k'

while True:
    try:
        count_arm = get_value(('10.8.10.223', 31010))
    except: pass
    try:
        count_amd = get_value(('amd-statefulset-0.amd-service.default.svc.cluster.local', 7777))
    except: pass

    print('ARM:', count_arm, 'AMD:', count_amd)

    sleep(interval)
