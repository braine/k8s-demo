from os import path, environ
import socket
import threading


# Function to load configuration and return server port,
# file to store results and last processed prime number
def load_configuration():
    try:
        # Read variables from script.conf
        r_file = environ.get('RESULTS_FILE')
        port = int(environ.get('SERVER_PORT'))
        # Create a results file if it doesn't exist
        if not path.exists(r_file):
            print("Creating a new result file")
            with open(r_file, "w") as rf:
                rf.write("0 2\n")
        # Get the last prime number processed by the script
        with open(r_file, 'r') as rf:
            num = int(rf.readline().split()[1])
        return r_file, port, num
    except Exception as e:
        print("Error loading configuration:", e)
        exit()


# Function to start a TCP server, listen for client
# requests on a separate thread and respond with the
# count value stored in the result file.
def start_tcp_server(port):
    # Open and listen on a tcp socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', port))
    s.listen(1)

    # Thread function to listen for clients
    def client_listener():
        while True:
            conn, _ = s.accept()
            try:
                with open(result_file, 'r') as rf:
                    conn.send(rf.readline().split()[0].encode())
                    conn.close()
            except Exception as e:
                print(e)

    # Thread to listen for clients and respond
    t = threading.Thread(target=client_listener)
    t.daemon = True
    t.start()

    # Return socket variable for graceful exit
    return s


# Function to find the next prime number and store the
# result (count number) in the result file.
def find_store_primes(num, r_file):
    for i in range(2, num):
        if (num % i) == 0:
            break
    else:
        with open(r_file, "r") as f:
            count = int(f.readline().split()[0])
        with open(r_file, "w") as f:
            f.write(str(count + 1) + ' ' + str(num))
        print("Total Prime numbers found =", count + 1)


if __name__ == "__main__":
    result_file, server_port, number = load_configuration()
    sock = start_tcp_server(server_port)
    try:
        while True:
            find_store_primes(number, result_file)
            number += 1
    except KeyboardInterrupt:
        print('interrupted!')
        sock.close()
